<?php
/**
* The database schema for our Data table
* @return the schema for use in Data table creation
*/
function usersteps_get_schema() {
  $data_table = new stdClass;
  $data_table->disabled = FALSE;
  $data_table->api_version = 1;
  $data_table->title = 'User Steps';
  $data_table->name = 'usersteps';
  $data_table->table_schema = array(
    'description' => '',
    'fields' => array(
      'uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'not null' => TRUE,
        'default' => 0,
        'description' => '',
      ),
      'step' => array(
        'type' => 'int',
        'size' => 'normal',
        'not null' => FALSE,
        'description' => '',
      ),
    ),
    'name' => 'userstates',
    'primary key' => array(),
    'indexes' => array(),
  );
  $data_table->meta = array(
    'fields' => array(
      'uid' => array(
        'label' => '',
      ),
      'step' => array(
        'label' => '',
      ),
    ),
  );

  return $data_table->table_schema;
}
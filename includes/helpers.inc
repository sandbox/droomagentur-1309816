<?php
include('data.inc'); // include the necessary functions for the Data module we utilize

/**
* Load the current steps for a given user, identified by UID
* @param anUid the user id of the user we want to load the steps for
* @return the current step of the user as an integer
*/
function usersteps_load($anUid) {
  global $user;
  
  $table = data_get_table('usersteps');
  if(!$table) {
    $table = data_create_table('usersteps', usersteps_get_schema()); 
  }
  $data = $table->handler()->load(array("uid" => $user->uid));

  return $data[0][step];
}

/**
* Assign a given step to a user (step number is optional) - this function also writes an entry into our log
* @param anUid the user id of the user we want to assign the steps to
* @param aStep the step number we want to assign (integer, optional)
*/
function usersteps_assign($anUid, $aStep = 1) {
  $table = data_get_table('usersteps');

  $data[$anUid] = array("uid" => $anUid, "step" => $aStep);

  $table->handler()->save($data[$anUid], array("uid"));
  watchdog('action', 'Assigned step %step to user with ID %uid.', array('%uid' => $anUid, '%step' => $aStep));
}

/**
* Advance the current step of a given user by one unless the maximum step level is already reached
* @param anUid the user id of the user we want to assign the steps to
*/
function usersteps_advance($anUid) {
  $step = usersteps_load($anUid);
  $stepcount = variable_get("usersteps_stepcount", 3);

  if(($step + 1) <= $stepcount) {
    $step++;
    usersteps_assign($anUid, $step);
  }
}

/**
* Reset a given user's steps
* @param anUid the user id of the user we want to have the steps reset
*/
function usersteps_reset($anUid) {
  usersteps_assign($anUid, 1);
}

/**
* Check if Data table exists - if not, let's create it ** UNTESTED **
*/
function usersteps_check_for_existing_table() {
  $table = data_get_table('usersteps');

  if(!$table) {
    $table = data_create_table('usersteps', usersteps_get_schema()); 
  }
}

/**
* Get the nextstep and link, optionally define our own next step
* @param nextStepText the text appearing on the link for the next step
* @param customNextStep a custom step number (integer) if we want to override the next step coming from our Data table. Step will also be written to user's Data table entry.
* @return the link (string)
*/
function usersteps_get_nextstep($nextStepText = "next &gt;&gt;", $customNextStep = NULL) {
  global $user;
  $nextStep = usersteps_load($user->uid + 1);

  if($customNextStep) {
    $nextStep = $customNextStep;
  }

  $nextStepNode = variable_get("usersteps_step_node_" . $nextStep, "/node" );
  $nextStepNode = token_replace($nextStepNode, $type = 'user', $object = NULL, $leading = '[', $trailing = ']');
  return "<a href=\"$nextStepNode\">$nextStepText</a>";
}
